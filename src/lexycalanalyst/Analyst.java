/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexycalanalyst;

import java.util.ArrayList;

public class Analyst {
    private String words;
    private int start;
    private ArrayList<String> string;
    private ArrayList<String> besaranLexic;
    private ArrayList<Character> tokenLexic;
    public Analyst(String words)
    {
        this.words = words+"&";
        start = 0;
        string = new ArrayList<String>();
        besaranLexic = new ArrayList<String>();
        tokenLexic = new ArrayList<Character>();
        initiator(0);
    }

    public ArrayList<String> getString() {
        return string;
    }
    
    public ArrayList<String> getBesaranLexic() {
        return besaranLexic;
    }

    public ArrayList<Character> getTokenLexic() {
        return tokenLexic;
    }

    public void initiator(int pos)
    {
        char _char = words.charAt(pos);
        if((_char>=65 && _char<=90)||(_char>=97 && _char<=122)) state1(++pos); //variable
        else if(_char=='+' || _char=='-' || _char=='*' || _char=='/') state2(++pos); //operator
        else if(_char>=48 && _char<=57) state3(++pos); //integer
        else if(_char=='(' || _char==')') state4(++pos); //grouping
        else if(_char=='&')return;
        else{
            start = pos+1;
            initiator(++pos);
        }
    }

    private void state1(int pos) { //state variable
        char _char = words.charAt(pos);
        if((_char>=65 && _char<=90)||(_char>=97 && _char<=122)||(_char>=48 && _char<=57)||(_char=='_')) state1(++pos);
        else{
            besaranLexic.add("Operand");
            tokenLexic.add('1');
            string.add(words.substring(start,pos));
            start=pos;
            initiator(pos);
        }
    }

    private void state2(int pos) {
        char _char = words.charAt(pos);
        besaranLexic.add("Operator");
        if(words.charAt(pos-1)=='+') tokenLexic.add('6');
        else if(words.charAt(pos-1)=='-') tokenLexic.add('7');
        else if(words.charAt(pos-1)=='*') tokenLexic.add('8');
        else if(words.charAt(pos-1)=='/') tokenLexic.add('9');
        string.add(words.substring(start,pos));
        start=pos;
        initiator(pos);
    }

    private void state3(int pos) {
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state3(++pos);
        else if(_char==',') state5(++pos);
        else if(_char=='E') state6(++pos);
        else{
            besaranLexic.add("Operand");
            tokenLexic.add('3');
            string.add(words.substring(start,pos));
            start=pos;
            initiator(pos);
        }
    }

    private void state4(int pos) {
        char _char = words.charAt(pos);
        besaranLexic.add("Grouping symbol");
        if(words.charAt(pos-1)=='(') tokenLexic.add('4');
        else if(words.charAt(pos-1)==')') tokenLexic.add('5');
        string.add(words.substring(start,pos));
        start = pos;
        initiator(pos);
    } 

    private void state5(int pos) {
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state7(++pos);
        else{
            if(_char=='&') return;
            start = pos+1;
            initiator(++pos);
        }
    }
    
    private void state6(int pos){
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state9(++pos);
        else if(_char=='+' || _char=='-') state8(++pos);
        else{
            besaranLexic.add("Operand");
            tokenLexic.add('2');
            string.add(words.substring(start,pos));
            start=pos;
            initiator(pos);
        }
    }

    private void state7(int pos) {
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state7(++pos);
        else if(_char=='E') state6(++pos);
        else{
            besaranLexic.add("Operand");
            tokenLexic.add('2');
            string.add(words.substring(start,pos));
            start=pos;
            initiator(pos);
        }
    }
    
    private void state8(int pos){
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state7(++pos);
        else{
            if(_char=='&') return;
            start = pos+1;
            initiator(++pos);
        }
    }
    
    private void state9(int pos){
        char _char = words.charAt(pos);
        if(_char>=48 && _char<=57) state9(++pos);
        else{
            besaranLexic.add("Operand");
            tokenLexic.add('2');
            string.add(words.substring(start,pos));
            start=pos;
            initiator(pos);
        }
    }
}
