/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexycalanalyst;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 *
 * @author wrismawan
 */
public class LexicalController implements KeyListener{

    LexicalGUI gui;
    private final JTextField txtlexical;
    private final JTextField txtToken;
    private JTable tbllexical;
    private Analyst analyst;
    private PDAAnalyst pda;
    private String words;
    private JLabel labelValid;

    public LexicalController(LexicalGUI gui) {
        this.gui = gui;
        txtlexical = gui.getjTextField1();
        txtToken = gui.getTxtToken();
        txtToken.addKeyListener(this);
        txtlexical.addKeyListener(this);
        tbllexical = gui.getjTable1();
        labelValid = gui.getLabelValid();
        labelValid.setText("");
    }

    public LexicalGUI getGui() {
        return gui;
    }

    public void setGui(LexicalGUI gui) {
        this.gui = gui;
    }

    public JTextField getTxtlexical() {
        return txtlexical;
    }

    public PDAAnalyst getPda() {
        return pda;
    }

    public void setPda(PDAAnalyst pda) {
        this.pda = pda;
    }

    public Analyst getAnalyst() {
        return analyst;
    }

    public void setAnalyst(Analyst analyst) {
        this.analyst = analyst;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }    
    
    @Override
    public void keyTyped(KeyEvent e) {
        int key = e.getKeyChar();
        if(key == 10){
            ArrayList<Character> tokenLexic;
            ArrayList<String> string;
            ArrayList<String> besaranLexic;
            if(e.getSource()==txtlexical){
                words = txtlexical.getText();
                analyst = new Analyst(words);
                tokenLexic = analyst.getTokenLexic();
                string = analyst.getString();
                besaranLexic = analyst.getBesaranLexic();
                pda = new PDAAnalyst(tokenLexic);
            }else{
                words = txtToken.getText();
                tokenLexic = new ArrayList<>();
                string = new ArrayList<>();
                besaranLexic = new ArrayList<>();
                for (char c : words.toCharArray()) {
                  tokenLexic.add(c);
                  string.add("-");
                  if(c=='1' || c=='2' || c=='3')
                      besaranLexic.add("Operand");
                  else if(c=='6'||c=='7'||c=='8'||c=='9')
                      besaranLexic.add("Operator");
                  else
                      besaranLexic.add("Grouping symbol");
                }
                pda = new PDAAnalyst(tokenLexic);
            }
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("String");
            model.addColumn("Besaran Lexic");
            model.addColumn("Token Lexic");
            for(int i=0;i<tokenLexic.size()-1;i++){
                model.addRow(new Object[]{string.get(i),besaranLexic.get(i),tokenLexic.get(i)});
            }
            tbllexical.setModel(model);
            if(pda.isStatus()){
                labelValid.setText("Valid");
                labelValid.setForeground(Color.green);
            }
            else{
                labelValid.setText("Invalid");
                labelValid.setForeground(Color.red);
            }
        }    
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    
}
