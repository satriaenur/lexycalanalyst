/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexycalanalyst;

import java.util.ArrayList;

/**
 *
 * @author wrismawan
 */
public class PDAAnalyst {
    private ArrayList<Character> token;
    private ArrayList<Character> stack = new ArrayList<>();
    boolean status = false;
    
    public PDAAnalyst(ArrayList<Character> token) {
        this.token = token;
        this.token.add('$');
        initiator(0);
    }

    public boolean isStatus() {
        return status;
    }
    
    public ArrayList<Character> getToken() {
        return token;
    }
    
    private void initiator(int pos){
        stack.add(0, '#');
        state1(pos);
    }
    
    private void state1(int pos){
        stack.add(0, 'S');
        state2(pos);
    }
    
    private void state2(int pos){
        char top = stack.remove(0);
        if(top=='#'&&token.get(pos)=='$')
            status = true;
        else{
            switch(top){
                case 'S':
                    if(token.get(pos)=='4'){
//                      Intuisi penentuan S+S atau (S
                        int jml = 1;
                        int a = pos+1;
                        char cek=' ';
                        while(jml!=0){
                            cek = token.get(a);
                            if(cek=='$') break;
                            if(cek=='4') jml++;
                            else if(cek=='5') jml--;
                            a++;
                        }
                        if(jml!=0) return;
                        if(token.get(a)=='$' || token.get(a)==stack.get(0)){
                            stack.add(0,'5');
                            stack.add(0,'S');
                            stack.add(0,'4');
                        }else if(token.get(a)=='6' || token.get(a)=='7' || token.get(a)=='8' || token.get(a)=='9'){
                            stack.add(0,'S');
                            stack.add(0,token.get(a));
                            stack.add(0,'S');
                        }else{
                            return;
                        }
//                      Intuisi done
                    }else{
                        if(token.get(pos)=='$') return;
//                      kalau ketemu token operand dan didepannya operator
                        if(token.get(pos+1)!=stack.get(0)&&(token.get(pos+1)!='$')){
                            stack.add(0,'S');
                            stack.add(0,token.get(pos+1));
                            stack.add(0,'S');
//                      case operator done
                        }else if((token.get(pos)=='1'||token.get(pos)=='2'||token.get(pos)=='3')&&
                                !(token.get(pos+1)=='1'||token.get(pos+1)=='2'||token.get(pos+1)!='3')){
                            stack.add(0,'P');
                        }else return;
                    }
                    state2(pos);
                    break;
                case 'P':
                    if(token.get(pos)=='1'||token.get(pos)=='2'||token.get(pos)=='3'){
                        stack.add(0,token.get(pos));
                        state2(pos);
                    }else return;
                    break;
                default:
                    if(top==token.get(pos)){
                        state2(++pos);
                    }else return;
            }
        }
    }
}
